unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j,t:integer;
    a:array[1..20,1..20] of integer;
    mins,mini,maxs,maxi:integer;
begin
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
      a[i,j]:=strtoint(StringGrid1.Cells[j-1,i-1]);
  mins:=100000;
  maxs:=-100000;
  for i:=1 to StringGrid1.RowCount do
  begin
    for j:=1 to StringGrid1.ColCount do
    begin
      if a[i,j]<mins then
      begin
        mins:=a[i,j];
        mini:=i;
      end;
      if a[i,j]>maxs then
      begin
        maxs:=a[i,j];
        maxi:=i;
      end;
    end;
  end;
  for j:=1 to StringGrid1.ColCount do
  begin
    t:=a[mini,j];
    a[mini,j]:=a[maxi,j];
    a[maxi,j]:=t;
  end;
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
      StringGrid1.Cells[j-1,i-1]:=IntToStr(a[i,j]);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  stringgrid1.RowCount:=StrToInt(Edit1.Text);
  stringgrid1.ColCount:=StrToInt(Edit2.Text);
end;

end.
