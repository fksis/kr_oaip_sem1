unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, math, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    Edit4: TEdit;
    Label5: TLabel;
    Edit5: TEdit;
    Button1: TButton;
    Chart1: TChart;
    Series1: TLineSeries;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

function f(x,a:extended;n:integer):extended;
var k,s:extended;
    i,j:integer;
    fac:longint;
begin
  k:=0;
  for i:=1 to n do
  begin
    fac:=1;
    s:=0;
    for j:=1 to n do
    begin
      fac:=fac*2*j*(2*j-1);
      s:=s+abs(sin(a*power(x,j)))/fac;
    end;
    k:=k+cos(s);
  end;
  result:=sqr(sin(k));
end;

procedure TForm1.Button1Click(Sender: TObject);
var y,a,xmin,xmax,hx,x:extended;
    n:integer;
begin
  memo1.Clear;
  chart1.SeriesList[0].Clear;
  a:=StrToFloat(Edit2.Text);
  n:=StrToInt(Edit1.Text);
  xmin:=StrToFloat(Edit3.Text);
  xmax:=StrToFloat(Edit4.Text);
  hx:=StrToFloat(edit5.Text);
  x:=xmin;
  while (x<xmax) or (abs(x-xmax)<0.0001) do
  begin
    y:=f(x,a,n);
    memo1.Lines.Add('x='+FloatToStrf(x,fffixed,4,2)+' f(x)='+FloatToStrF(y,fffixed,4,4));
    chart1.SeriesList[0].AddXY(x,y);
    x:=x+hx;
  end;
end;

end.
