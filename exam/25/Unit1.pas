unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label2: TLabel;
    Edit1: TEdit;
    Button2: TButton;
    Memo1: TMemo;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Math;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j,t,z:integer;
    k:extended;
    a:array[1..20,1..2] of integer;
    dind:array[1..2,1..400] of integer;
    dist:array[1..400] of extended;
begin
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
      a[i,j]:=strtoint(StringGrid1.Cells[j-1,i-1]);
  t:=1;
  for i:=1 to StringGrid1.RowCount-1 do
  begin
    for j:=i+1 to StringGrid1.RowCount do
    begin
      dind[1,t]:=i;
      dind[2,t]:=j;
      dist[t]:=sqrt(sqr(a[i,1]-a[j,1])+sqr(a[i,2]-a[j,2]));
      t:=t+1;
    end;
  end;
  for i:=1 to t-1 do
    for j:=1 to t-2 do
    begin
      if (dist[j]<dist[j+1]) then
      begin
        k:=dist[j];
        dist[j]:=dist[j+1];
        dist[j+1]:=k;
        z:=dind[1,j];
        dind[1,j]:=dind[1,j+1];
        dind[1,j+1]:=z;
        z:=dind[2,j];
        dind[2,j]:=dind[2,j+1];
        dind[2,j+1]:=z;
      end;
    end;
  for i:=1 to Min(5,t-1) do
  begin
    memo1.Lines.Add('����� ('+inttostr(a[dind[1,i],1])+', '+inttostr(a[dind[1,i],2])+') � ('+inttostr(a[dind[2,i],1])+', '+inttostr(a[dind[2,i],2])+'): ���������� '+floattostrf(dist[i],fffixed,4,2));
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  stringgrid1.RowCount:=StrToInt(Edit1.Text);
end;

end.
