unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Button2: TButton;
    Label4: TLabel;
    Edit3: TEdit;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button2Click(Sender: TObject);
begin
  StringGrid1.RowCount:=strtoint(edit1.text);
  StringGrid1.ColCount:=strtoint(edit2.text);
end;

procedure TForm1.Button1Click(Sender: TObject);
var i,j,sum, cnt:integer; oa, os: extended;
    a:array[1..20,1..20] of integer;
begin
  sum:=0;
  cnt:=0;
  os:=0;
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.RowCount do
    begin
      a[i,j]:=StrToInt(StringGrid1.Cells[j-1,i-1]);
      sum:=sum+a[i,j];
    end;
  oa:=sum/(StringGrid1.RowCount*StringGrid1.ColCount);
  for i:=1 to StringGrid1.RowCount do
  begin
    sum:=0;
    for j:=1 to StringGrid1.ColCount do
      sum:=sum+a[i,j];
    os:=sum/StringGrid1.ColCount;
    if os>oa then
      cnt:=cnt+1;
  end;
  Edit3.Text:=IntToStr(cnt);
end;

end.
