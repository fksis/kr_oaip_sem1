unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    StringGrid1: TStringGrid;
    Button1: TButton;
    Label2: TLabel;
    Edit1: TEdit;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button2Click(Sender: TObject);
begin
  StringGrid1.RowCount:=StrToInt(Edit1.Text);
  StringGrid1.ColCount:=StrToInt(Edit1.Text);
end;

procedure TForm1.Button1Click(Sender: TObject);
var i,j,maxv,maxi,maxj,minv,mini,minj,t:integer;
    a:array[1..20,1..20] of integer;
begin
  maxv:=-111111;
  minv:=111111;
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
    begin
      a[i,j]:=strtoint(StringGrid1.Cells[j-1,i-1]);
      if (a[i,j]>maxv) then
      begin
        maxi:=i;
        maxj:=j;
        maxv:=a[i,j];
      end;
      if (a[i,j]<minv) then
      begin
        mini:=i;
        minj:=j;
        minv:=a[i,j];
      end;
    end;
  for i:=1 to StringGrid1.RowCount do
  begin
    t:=a[i,minj];
    a[i,minj]:=a[i,maxj];
    a[i,maxj]:=t;
  end;
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
      StringGrid1.Cells[j-1,i-1]:=IntToStr(a[i,j]);
end;

end.
