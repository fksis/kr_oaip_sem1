unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, math;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label2: TLabel;
    Edit1: TEdit;
    Button2: TButton;
    Label3: TLabel;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j,t,minv,mini,maxv,maxi:integer;
    a:array[1..20] of integer;
begin
  maxv:=-11111;
  minv:=11111;
  for i:=1 to StringGrid1.ColCount do
  begin
    a[i]:=strtoint(StringGrid1.Cells[i-1,0]);
    if (a[i]>maxv) then
    begin
      maxv:=a[i];
      maxi:=i;
    end;
    if (a[i]<minv) then
    begin
      minv:=a[i];
      mini:=i;
    end;
  end;
  t:=0;
  for i:=min(mini,maxi)+1 to max(mini,maxi)-1 do
  begin
    t:=t+a[i];
  end;
  edit2.Text:=IntToStr(t);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  stringgrid1.ColCount:=StrToInt(Edit1.Text);
end;

end.
