unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
const n=5; m=4;
type Mas=array[1..m,1..n] of extended;
var A:Mas;
    i,j:integer;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  Randomize;
  for i:=1 to m do
  for j:=1 to n do
  StringGrid1.Cells[i-1,j-1]:=floattostr(random(101)-50);
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
 var max,min:extended;
 begin
  max:=A[m,n];
  min:=A[m,n];
  for i:=1 to m do
  for j:=1 to n do
   begin
     A[i,j]:=StrToFloat(StringGrid1.Cells[i-1,j-1]);
     if A[i,j]>max then max:=A[i,j];
     if A[i,j]<min then min:=A[i,j];
     A[i,j]:=A[i,j]*2/(min+max);
     StringGrid2.Cells[i-1,j-1]:=FloattoStrF(A[i,j],fffixed,4,2);
   end;

 end;

end.



