object Form1: TForm1
  Left = 191
  Top = 124
  Width = 434
  Height = 480
  Caption = #1042#1072#1088#1080#1072#1085#1090' 12'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 12
    Height = 13
    Caption = 'n='
  end
  object Label2: TLabel
    Left = 120
    Top = 8
    Width = 12
    Height = 13
    Caption = 'a='
  end
  object Label3: TLabel
    Left = 8
    Top = 32
    Width = 29
    Height = 13
    Caption = 'Xmin='
  end
  object Label4: TLabel
    Left = 96
    Top = 32
    Width = 32
    Height = 13
    Caption = 'Xmax='
  end
  object Label5: TLabel
    Left = 16
    Top = 56
    Width = 19
    Height = 13
    Caption = 'Hx='
  end
  object Edit1: TEdit
    Left = 40
    Top = 8
    Width = 49
    Height = 21
    TabOrder = 0
    Text = '3'
  end
  object Edit2: TEdit
    Left = 136
    Top = 8
    Width = 49
    Height = 21
    TabOrder = 1
    Text = '2'
  end
  object Edit3: TEdit
    Left = 40
    Top = 32
    Width = 49
    Height = 21
    TabOrder = 2
    Text = '0,1'
  end
  object Edit4: TEdit
    Left = 136
    Top = 32
    Width = 49
    Height = 21
    TabOrder = 3
    Text = '2'
  end
  object Edit5: TEdit
    Left = 40
    Top = 56
    Width = 49
    Height = 21
    TabOrder = 4
    Text = '0,1'
  end
  object Button1: TButton
    Left = 192
    Top = 8
    Width = 217
    Height = 65
    Caption = #1058#1072#1073#1091#1083#1080#1088#1086#1074#1072#1090#1100
    TabOrder = 5
    OnClick = Button1Click
  end
  object Chart1: TChart
    Left = 8
    Top = 80
    Width = 400
    Height = 250
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    View3D = False
    TabOrder = 6
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Title = 'f(x)'
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object Memo1: TMemo
    Left = 8
    Top = 336
    Width = 401
    Height = 97
    ScrollBars = ssVertical
    TabOrder = 7
  end
end
