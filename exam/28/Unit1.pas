unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    Memo2: TMemo;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j:integer;
begin
  if OpenDialog1.Execute then
  begin
    memo1.Lines.LoadFromFile(OpenDialog1.FileName);
    for i:=0 to memo1.Lines.Count-1 do
    begin
      memo2.Lines.Add(StringReplace(memo1.lines[i],'begin','start',[rfReplaceAll,rfIgnoreCase]));
    end;
  end;
end;

end.
