unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label2: TLabel;
    Edit1: TEdit;
    Button2: TButton;
    Label3: TLabel;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j,max,t:integer;
    a:array[1..20,1..20] of integer;
begin
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
      a[i,j]:=strtoint(StringGrid1.Cells[j-1,i-1]);
  max:=-11111;
  t:=0;
  for i:=(StringGrid1.RowCount div 2 + 1) to StringGrid1.RowCount do
  begin
    for j:=(StringGrid1.ColCount div 2 + StringGrid1.ColCount mod 2 - t) to StringGrid1.ColCount-(StringGrid1.ColCount div 2 + StringGrid1.ColCount mod 2 - t)+1 do
    begin
      if a[i,j]>max then
        max:=a[i,j];
    end;
    t:=t+1;
  end;
  edit2.Text:=IntToStr(max);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  stringgrid1.RowCount:=StrToInt(Edit1.Text);
  stringgrid1.ColCount:=StrToInt(Edit1.Text);
end;

end.
