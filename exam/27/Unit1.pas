unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label3: TLabel;
    Edit2: TEdit;
    Button2: TButton;
    StringGrid2: TStringGrid;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j,t:integer;
    a,b:array[1..20] of integer;
begin
  for i:=1 to StringGrid1.ColCount do
    a[i]:=strtoint(StringGrid1.Cells[i-1,0]);
  t:=1;
  for i:=1 to StringGrid1.ColCount do
    if a[i]<0 then
    begin
      b[t]:=a[i];
      t:=t+1;
    end;
  for i:=1 to StringGrid1.ColCount do
    if a[i]>=0 then
    begin
      b[t]:=a[i];
      t:=t+1;
    end;
  for i:=1 to StringGrid1.ColCount do
    StringGrid2.Cells[i-1,0]:=IntToStr(b[i]);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  stringgrid1.ColCount:=StrToInt(Edit2.Text);
end;

end.
