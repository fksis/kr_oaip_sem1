unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j,t,z:integer;
    a:array[1..20,1..20] of integer;
    sums:array[1..2,1..20] of integer;
begin
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
      a[i,j]:=strtoint(StringGrid1.Cells[j-1,i-1]);
  for i:=1 to StringGrid1.RowCount do
  begin
    for j:=1 to StringGrid1.ColCount do
      for t:=1 to StringGrid1.ColCount-1 do
      begin
        if (a[i,t]>a[i,t+1]) then
        begin
          z:=a[i,t];
          a[i,t]:=a[i,t+1];
          a[i,t+1]:=z;
        end;
      end;
  end;
  for i:=1 to stringgrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
    begin
      StringGrid1.Cells[j-1,i-1]:=inttostr(a[i,j]);
    end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  stringgrid1.RowCount:=StrToInt(Edit1.Text);
  stringgrid1.ColCount:=StrToInt(Edit2.Text);
end;

end.
