unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Button2: TButton;
    Label4: TLabel;
    StringGrid2: TStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,j,cnt,x1,x2,y1,y2:integer;
    a:array[1..20,1..20] of integer;
begin
  for i:=1 to StringGrid1.RowCount do
    for j:=1 to StringGrid1.ColCount do
      a[i,j]:=strtoint(StringGrid1.Cells[j-1,i-1]);
  cnt:=0;
  x1:=1;
  x2:=StringGrid1.ColCount;
  y1:=1;
  y2:=StringGrid1.RowCount;
  while cnt<StringGrid1.RowCount*StringGrid1.ColCount do
  begin
    for i:=x1 to x2 do
    begin
      StringGrid2.Cells[cnt,0]:=IntToStr(a[y1,i]);
      cnt:=cnt+1;
    end;
    y1:=y1+1;
    if cnt>=StringGrid1.RowCount*StringGrid1.ColCount then exit;

    for i:=y1 to y2 do
    begin
      StringGrid2.Cells[cnt,0]:=IntToStr(a[i,x2]);
      cnt:=cnt+1;
    end;
    x2:=x2-1;
    if cnt>=StringGrid1.RowCount*StringGrid1.ColCount then exit;

    for i:=x2 downto x1 do
    begin
      StringGrid2.Cells[cnt,0]:=IntToStr(a[y2,i]);
      cnt:=cnt+1;
    end;
    y2:=y2-1;
    if cnt>=StringGrid1.RowCount*StringGrid1.ColCount then exit;

    for i:=y2 downto y1 do
    begin
      StringGrid2.Cells[cnt,0]:=IntToStr(a[i,x1]);
      cnt:=cnt+1;
    end;
    x1:=x1+1;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  stringgrid1.RowCount:=StrToInt(Edit1.Text);
  stringgrid1.ColCount:=StrToInt(Edit2.Text);
  StringGrid2.ColCount:=StrToInt(Edit1.Text)*StrToInt(Edit2.Text);
end;

end.
