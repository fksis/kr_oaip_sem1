unit Unit2;
interface
Function Rut(const a,e:extended):extended;
Implementation
Function Rut;
Var x1,x2,d:extended;
Begin 
     x1:=1;
     repeat
       x2:=(x1*(cos(x1*pi)))/x1;
       d:=abs(x2-x1);
       x1:=x2;
     until d<e;
Result:=x2;
End;


end.
 