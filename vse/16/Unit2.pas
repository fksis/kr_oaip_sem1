unit Unit2;

interface
function min(a,b,h:extended):extended;

implementation
function min(a,b,h:extended):extended;
var i,minimal:extended;
begin
i:=a;
minimal:=sqr(a-1);
while i<=b do
  begin
    if sqr(i-1)<minimal then minimal:=sqr(i-1);
    i:=i+h;
  end;
Result:=minimal;
end;

end.
 