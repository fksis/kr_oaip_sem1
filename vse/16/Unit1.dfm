object Form1: TForm1
  Left = 171
  Top = 150
  Width = 368
  Height = 121
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 6
    Height = 13
    Caption = 'a'
  end
  object Label2: TLabel
    Left = 16
    Top = 32
    Width = 6
    Height = 13
    Caption = 'b'
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 6
    Height = 13
    Caption = 'h'
  end
  object Label4: TLabel
    Left = 72
    Top = 8
    Width = 266
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1100#1096#1077#1077' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1092#1091#1085#1082#1094#1080#1080' '#1085#1072' '#1087#1088#1086#1084#1077#1078#1091#1090#1082#1077' [a,b]'
  end
  object Edit1: TEdit
    Left = 24
    Top = 8
    Width = 33
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 24
    Top = 32
    Width = 33
    Height = 21
    TabOrder = 1
    Text = 'Edit2'
  end
  object Edit3: TEdit
    Left = 24
    Top = 56
    Width = 33
    Height = 21
    TabOrder = 2
    Text = 'Edit3'
  end
  object Edit4: TEdit
    Left = 72
    Top = 24
    Width = 265
    Height = 21
    TabOrder = 3
    Text = 'Edit4'
  end
  object Button1: TButton
    Left = 152
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Go'
    TabOrder = 4
    OnClick = Button1Click
  end
end
