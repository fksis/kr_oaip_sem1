unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Unit2;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit4: TEdit;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
Edit1.Text:='-10';
Edit2.Text:='100';
Edit3.Text:='1';
Edit4.Text:='';
end;

procedure TForm1.Button1Click(Sender: TObject);
var a,b,h:extended;
begin
a:=StrToFloat(Edit1.Text);
b:=StrToFloat(Edit2.Text);
h:=StrToFloat(Edit3.Text);
Edit4.Text:=FloatToStr(min(a,b,h));
end;

end.
