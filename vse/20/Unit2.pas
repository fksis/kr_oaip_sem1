unit Unit2;

interface
function nok(m,n:integer):integer;
function nod(m,n:integer):integer;

implementation
function nok(m,n:integer):integer;
var
  i:integer;
begin
i:=m;
while ((i mod m<>0) or (i mod n<>0)) do
  begin
    i:=i+1;
  end;
   result:=i;
end;

function nod(m,n:integer):integer;
var i:integer;
begin
if n>m then i:=n else i:=m;
  while ((m mod i  <> 0) or (n mod i<>0)) do
  begin
  i:=i-1;
  end;
  result:=i;
end;
end.
 