object Form1: TForm1
  Left = 192
  Top = 125
  Width = 870
  Height = 500
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object strngrd1: TStringGrid
    Left = 120
    Top = 112
    Width = 320
    Height = 120
    ColCount = 4
    RowCount = 4
    TabOrder = 0
  end
  object strngrd2: TStringGrid
    Left = 496
    Top = 112
    Width = 105
    Height = 120
    ColCount = 1
    FixedCols = 0
    RowCount = 4
    TabOrder = 1
  end
  object strngrd3: TStringGrid
    Left = 624
    Top = 112
    Width = 121
    Height = 120
    ColCount = 1
    FixedCols = 0
    RowCount = 4
    TabOrder = 2
  end
  object btn1: TButton
    Left = 376
    Top = 72
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 3
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 392
    Top = 352
    Width = 75
    Height = 25
    Caption = 'btn2'
    TabOrder = 4
    OnClick = btn2Click
  end
  object edt1: TEdit
    Left = 160
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'edt1'
  end
  object edt2: TEdit
    Left = 160
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'edt2'
  end
  object btn3: TButton
    Left = 400
    Top = 240
    Width = 75
    Height = 25
    Caption = 'btn3'
    TabOrder = 7
    OnClick = btn3Click
  end
end
