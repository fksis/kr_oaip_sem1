unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit2, StdCtrls;

type
  TForm1 = class(TForm)
    btn1: TButton;
    edt1: TEdit;
    edt2: TEdit;
    mmo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Edt1.Text := '1';
  Edt2.Text := '2';
  Edt3.Text := '';

end;

procedure TForm1.btn1Click(Sender: TObject);
 Var a,e,qa:extended;

Begin
 a:=StrtoFloat(Edt1.Text);
 e:=StrtoFloat(Edt2.Text);
 qa:=Rut(a,e);
 mmo1.Lines.Add('predel='+FloatToStrF(qa,ffFixed,6,2));
end;

end.
