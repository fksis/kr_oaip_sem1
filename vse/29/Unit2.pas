unit Unit2;

interface
Function Rut(const a,e:extended):extended;
implementation
Function Rut;
Var x1,x2,d:extended;
Begin 
     x1:=1;
     repeat
       x2:=0.5*(x1+a/x1);
       d:=abs(x2-x1);
       x1:=x2;
     until d<e;
Result:=x2;
End;
End.


