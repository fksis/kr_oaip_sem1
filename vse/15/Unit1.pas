unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    edt4: TEdit;
    btn1: TButton;
    img1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
edt1.text:='0';
edt2.Text:='0';
edt3.Text:='10';
img1.Picture:=nil;
end;

procedure TForm1.btn1Click(Sender: TObject);
var
  hx: Extended;
  xx,yy,rr:Extended;
  xo,yo,xmax,ymax,x,y,r: LongInt;
  begin
    xx:=StrToFloat(edt1.text);
    yy:=StrToFloat(edt2.Text);
    rr:=StrToFloat(edt3.text);
    xmax:=img1.Width;
    ymax:=img1.Height;
    xo:=xmax div 2;
    yo:=ymax div 2;
    hx:=StrToFloat(edt4.text);
    x:= Round(xo + xx/hx);
  y := Round(yo + yy/hx);
  r := Round(rr/hx);
  with img1.Canvas do begin
    //2*r = (2/3)xmax
    //hx := (2*r) / (2/3*xmax);
   // Ellipse(xo-r/hx,yo+)
    Pen.Width := 4;
    Pen.Color := clBlack;
    Brush.Color := clWhite;
    Rectangle(0,0,xmax,ymax);
    Brush.Color := clGreen;
    Pen.Color := clRed;
    Ellipse(xo - r,yo - r,xo + r,yo + r);
  end;
end;
end.


