unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

procedure StringCheck(s: string; edit: TEdit);

implementation

procedure StringCheck(s: string; edit: TEdit);
var
  i, m1, m2: integer;
  substr: string;
begin
  m2 := 0;

  m1 := pos('[', s);
  for i := 1 to Length(s) do
    if s[i] = ']' then
      m2 := i;

  if (m1 > m2) or (m1 = 0) or (m2 = 0) then
  begin
    Edit.Text := '������ �����!';
    Exit;
  end;

  for i := m1 to m2 do
    substr := substr + s[i];
  Edit.Text := substr;

end;

end.
 