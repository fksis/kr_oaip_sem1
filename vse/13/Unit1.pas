unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Unit2;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    edt4: TEdit;
    btn1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  edt1.Text := '0';
  Edt2.Text := '1';
  Edt3.Text := '100';
  Edt4.Text := '';
end;
function sx( x  : Extended ) : Extended;
begin
  sx := Sqr(sin(x));
end;


procedure TForm1.btn1Click(Sender: TObject);
var
  a,b : Extended;
  n : LongInt;
  ans : Extended;
begin
  a := StrToFloat(Edt1.Text);
  b := StrToFloat(Edt2.Text);
  n := StrToInt(Edt3.Text);
  ans := solve(a,b,n,sx);
  Edt4.Text := FloatToStrF(ans,ffFixed,8,3);
end;

end.
