object Form1: TForm1
  Left = 192
  Top = 124
  Width = 201
  Height = 469
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 7
    Height = 13
    Caption = 'K'
  end
  object Label2: TLabel
    Left = 24
    Top = 40
    Width = 9
    Height = 13
    Caption = 'M'
  end
  object Memo1: TMemo
    Left = 24
    Top = 72
    Width = 137
    Height = 289
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 40
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 40
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit2'
  end
  object Button1: TButton
    Left = 56
    Top = 376
    Width = 75
    Height = 25
    Caption = 'Go'
    TabOrder = 3
    OnClick = Button1Click
  end
end
