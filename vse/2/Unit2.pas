unit Unit2;

interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

procedure StringCheck(s: string; memo: TMemo);

implementation

procedure StringCheck(s: string; memo: TMemo);
Type
  CharSet = set of char;
var
  chs: CharSet;
  i, index, code: integer;
  substr: string;
begin
  chs := ['0' .. '9'];
  substr := '';

  for i := 1 to Length(s) do
    if not(s[i] in chs) and (s[i] <> ' ') then
    begin
      Memo.Lines.Add('������ �����!');
      Exit;
    end;

  i := 1;

  while i <= Length(s) do
  begin
    while s[i] <> ' ' do
    begin
      substr := substr + s[i];
      Inc(i, 1);
    end;
    val(substr, index, code);
    if Odd(index) = false then
      Memo.Lines.Add(IntToStr(index));
    Inc(i, 1);
    substr := '';
  end;

end;

end.
