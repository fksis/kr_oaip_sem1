unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, Math;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Button1: TButton;
    Memo1: TMemo;
    Label1: TLabel;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

var a:array[1..5] of integer;
{$R *.dfm}

procedure gorner(x,n:integer);
var i,b:integer;
begin
  b:=0;
  for i:=n downto 1 do
  begin
    b:=a[i]+b*x;
  end;

  form1.Memo1.Lines.Add('P(x)='+IntToStr(b));
end;

procedure TForm1.Button1Click(Sender: TObject);
var i:integer;
begin
  for i:=1 to StringGrid1.ColCount do
    a[i]:=strtoint(StringGrid1.Cells[i-1,0]);
  gorner(StrToInt(edit1.Text),StringGrid1.ColCount);
end;

end.
