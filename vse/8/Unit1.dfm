object Form1: TForm1
  Left = 988
  Top = 108
  Width = 332
  Height = 502
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 424
    Width = 6
    Height = 13
    Caption = 'n'
  end
  object Label2: TLabel
    Left = 8
    Top = 328
    Width = 6
    Height = 13
    Caption = 'k'
  end
  object Label3: TLabel
    Left = 8
    Top = 352
    Width = 8
    Height = 13
    Caption = 'm'
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 313
    Height = 313
    ColCount = 6
    DefaultColWidth = 50
    DefaultRowHeight = 50
    FixedCols = 0
    RowCount = 6
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 0
  end
  object RadioGroup1: TRadioGroup
    Left = 224
    Top = 320
    Width = 89
    Height = 89
    Caption = #1052#1077#1085#1103#1077#1084':'
    Items.Strings = (
      #1057#1090#1088#1086#1082#1080
      #1057#1090#1086#1083#1073#1094#1099)
    TabOrder = 1
  end
  object Button1: TButton
    Left = 0
    Top = 384
    Width = 169
    Height = 25
    Caption = 'Go'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 24
    Top = 328
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 24
    Top = 352
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'Edit2'
  end
  object Edit3: TEdit
    Left = 224
    Top = 424
    Width = 89
    Height = 21
    TabOrder = 5
    Text = 'Edit3'
  end
  object Button2: TButton
    Left = 0
    Top = 424
    Width = 169
    Height = 25
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1088#1072#1079#1084#1077#1088#1085#1086#1089#1090#1100
    TabOrder = 6
    OnClick = Button2Click
  end
end
