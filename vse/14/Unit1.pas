unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    edt4: TEdit;
    edt5: TEdit;
    edt6: TEdit;
    edt7: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    img1: TImage;
    btn1: TButton;
    btn2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Edt1.Text := '120';
  Edt2.Text := '50';
  Edt3.Text := '70';
  Edt4.Text := '90';
  Edt5.Text := '180';
  Edt6.Text := '100';
  Edt7.Text := '1';
  img1.Picture := nil;

end;

procedure TForm1.btn1Click(Sender: TObject);
var
  p : array[1..3] of TPoint;
  h : Extended;
begin
  img1.Picture :=  nil;
  with img1.Canvas do begin
    Pen.Color := clBlue;
    Brush.Color := clYellow;
    h := StrToFloat(Edt7.Text);
    p[1].x := Round(StrToFloat(Edt1.Text)/h);
    p[1].Y := Round(StrToFloat(Edt2.Text)/h);
    p[2].x := Round(StrToFloat(Edt3.Text)/h);
    p[2].Y := Round(StrToFloat(Edt4.Text)/h);
    p[3].x := round(StrTofloat(Edt5.Text)/h);
    p[3].y := Round(StrToFloat(Edt6.Text)/h);
    Polygon(p);                             
  end;
end;


end.
