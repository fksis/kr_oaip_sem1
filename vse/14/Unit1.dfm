object Form1: TForm1
  Left = 253
  Top = 185
  Width = 870
  Height = 500
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 64
    Top = 32
    Width = 16
    Height = 13
    Caption = 'lbl1'
  end
  object lbl2: TLabel
    Left = 64
    Top = 72
    Width = 16
    Height = 13
    Caption = 'lbl2'
  end
  object lbl3: TLabel
    Left = 64
    Top = 104
    Width = 16
    Height = 13
    Caption = 'lbl3'
  end
  object lbl4: TLabel
    Left = 64
    Top = 136
    Width = 16
    Height = 13
    Caption = 'lbl4'
  end
  object lbl5: TLabel
    Left = 64
    Top = 184
    Width = 16
    Height = 13
    Caption = 'lbl5'
  end
  object lbl6: TLabel
    Left = 72
    Top = 216
    Width = 16
    Height = 13
    Caption = 'lbl6'
  end
  object lbl7: TLabel
    Left = 72
    Top = 248
    Width = 16
    Height = 13
    Caption = 'lbl7'
  end
  object img1: TImage
    Left = 248
    Top = 32
    Width = 249
    Height = 153
  end
  object edt1: TEdit
    Left = 104
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'edt1'
  end
  object edt2: TEdit
    Left = 104
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'edt2'
  end
  object edt3: TEdit
    Left = 104
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'edt3'
  end
  object edt4: TEdit
    Left = 104
    Top = 136
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'edt4'
  end
  object edt5: TEdit
    Left = 104
    Top = 176
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'edt5'
  end
  object edt6: TEdit
    Left = 104
    Top = 208
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'edt6'
  end
  object edt7: TEdit
    Left = 104
    Top = 240
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'edt7'
  end
  object btn1: TButton
    Left = 240
    Top = 360
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 7
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 424
    Top = 320
    Width = 75
    Height = 25
    Caption = 'btn2'
    TabOrder = 8
  end
end
